(function(){
    angular
        .module('DonationsApp')
        .service('LoginService', loginService)
        loginService.$inject = ['$cookies']
        function loginService($cookies){
            
            vm = this;
            vm.setToken = setToken;
            vm.getToken = getToken;
            vm.setDonor = setDonor;
            vm.getDonor = getDonor;


            function setToken (tokenApp){
                return $cookies.tokenApp = tokenApp; 
            }

            function getToken(){
                return $cookies.tokenApp;
            }

            function setDonor(tokenDonor){
                return $cookies.tokenDonor = tokenDonor;
            }

            function getDonor(){
                return $cookies.tokenDonor;
            }
            
        }
})();