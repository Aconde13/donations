(function(){
    'use strict';
    angular
        .module('DonationsApp')
        .factory('DonationFactory', donationFactory);

        donationFactory.$inject = ['$http', 'apiKey', 'LoginService', 'api', 'DonationService'];
        function donationFactory($http, apiKey, LoginService, api, DonationService){
            vm = this;

            return {
                getPaymentTypeConfiguration: function getPaymentTypeConfiguration(){
                    return $http(
                        {
                            'method': 'GET', 
                            'url': api+'/api/Configuration/PaymentTypeConfiguration',
                            'data' : {},
                            'params' : {
                                'apikey' : apiKey,
                                'paymentType': '2',
                                'donorToken': LoginService.getDonor()
                            
                            },
                            'headers': {
                            'Authorization': 'Bearer "'+ LoginService.getToken()+'"',
                            'apiKey': apiKey
                            }
                        }).then(function(response){
                            return response
                        }).catch(function(error){
                            return error
                        })
                },
                
                getContries: function getContries(){
                    return $http(
                        {
                            'method': 'GET', 
                            'url': api+'/api/Configuration/Countries',
                            'data' : {},
                            'params' : {
                                'apikey' : apiKey
                            },
                            'headers': {
                                'Authorization': 'Bearer "'+ LoginService.getToken()+'"',
                                'apiKey': apiKey
                            }
                        }).then(function(response){
                            return response
                        }).catch(function(error){
                            return error
                        })
                },
                
                getStates: function getStates(){
                    return $http(
                        {
                            'method': 'GET', 
                            'url': api+'/api/Configuration/USStates',
                            'data' : {},
                            'params' : {
                                'apikey' : apiKey
                            },
                            'headers': {
                                'Authorization': 'Bearer "'+ LoginService.getToken()+'"',
                                'apiKey': apiKey
                            }
                        }).then(function(response){
                            return response
                        }).catch(function(error){
                            return error
                        })
                },

                getAgencies: function getAgencies(){
                    return $http(
                        {
                            'method': 'GET', 
                            'url': api+'/api/Configuration/IntroductoryPanel',
                            'data' : {},
                            'params' : {
                                'apikey' : apiKey,
                                'donorToken': LoginService.getDonor()
                            },
                            'headers': {
                                'Authorization': 'Bearer "'+ LoginService.getToken()+'"',
                                'apiKey': apiKey
                            }
                        }).then(function(response){
                            return response
                        }).catch(function(error){
                            return error
                        })
                },

                setDonation: function setDonation(){
                    return $http({
                        'method': 'POST',
                        'url': api+'/api/Donation/Save',
                        'data' : [
                            {
                                'AddonTotalList': [],
                                'AddOnTotalValue': 0,
                                'CampaignId': 'c4f85b7e-54eb-4194-bcb1-b0a072217e09',
                                'CustomField1': DonationService.getFirstStep().cardType,
                                'CustomField2': DonationService.getFirstStep().nameOnCard,
                                'CustomField3': DonationService.getFirstStep().cardNumber,
                                'CustomField4': DonationService.getFirstStep().expirationDate,
                                'CustomField5': DonationService.getFirstStep().email,
                                'CustomField6': DonationService.getFirstStep().cvn,
                                'DesignationAmountType': 1,
                                'DesignationList': [
                                    {
                                      'DesignateableEntityType': DonationService.getSecondStep().designateableEntityType,
                                      'DesignationAmount': DonationService.getFirstStep().amount,
                                      'DisplayName': '',
                                      'EIN': DonationService.getSecondStep().ein,
                                      'EntityId': DonationService.getSecondStep().entityId,
                                      'IsDefaultPanelItem': false,
                                      'IsRejected': false,
                                      'MinimumDonation': DonationService.getSecondStep().minimumDonation,
                                      'MinimumTotalDonationForDesignation': DonationService.getSecondStep().minimumTotalDonationForDesignation,
                                      'Name': DonationService.getSecondStep().name,
                                      'OrganizationNumber': DonationService.getSecondStep().organizationNumber,
                                      'StandardAccountCode': DonationService.getSecondStep().standardAccountCode
                                    }
                                  ],
                                'DesignationWriteInList': [],
                                'DonationSourceType': 9,
                                'FrequencyType': 1,
                                'ImpersonatedUser': '',
                                'IsConfirmed': true,
                                'IsImpersonated': false,
                                'NegativeDesignation': '',
                                'Payment': {
                                      'BillingAddress1' : '',
                                      'BillingAddress2' : '',
                                      'BillingCity' : '',
                                      'BillingState' : '',
                                      'BillingZipCode' : '',
                                      'BillingZipCodeExt' : ''
                                },
                                'PaymentAmount': DonationService.getFirstStep().amount,
                                'PaymentAmountType': 1,
                                'PaymentIncreaseAmount': 0,
                                'PaymentIncreaseAmountType': 1,
                                'PaymentTotalValue': DonationService.getFirstStep().amount,
                                'PaymentType': 5,
                                'PledgeStatusType': 0,
                                'TotalValue': DonationService.getFirstStep().amount
                            }
                        ],
                        'params' : {
                            'apikey' : apiKey,
                            'donorToken': LoginService.getDonor(),
                            'ipAddress': '192.168.2.42'
                        
                        },
                        'headers': {
                        'Authorization': 'Bearer "'+ LoginService.getToken()+'"',
                        'apiKey': apiKey,
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                        }
                    }).then(function(response){
                        return response
                    }).catch(function(error){
                        return error
                    })
                },

                getDonations: function getDonations(){
                    return $http(
                        {
                            'method': 'GET', 
                            'url': api+'/api/Donor/GivingHistory',
                            'data' : {},
                            'params' : {
                                'apikey' : apiKey,
                                'donorToken': LoginService.getDonor()
                            },
                            'headers': {
                                'Authorization': 'Bearer "'+ LoginService.getToken()+'"',
                                'apiKey': apiKey
                            }
                        }).then(function(response){
                            return response
                        }).catch(function(error){
                            return error
                        })
                }

            }
        }
})();