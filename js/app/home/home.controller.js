(function(){
    angular
        .module('DonationsApp')
        .controller('HomeController', homeController);
    
    homeController.$inject = ['LoginService'];
    function homeController(LoginService){
        vm = this;
        
        if(LoginService.getDonor()){
            vm.tokenDonor = LoginService.getDonor();
        }else{
            vm.tokenDonor = '-1';
        }

    }
})();