(function(){
    angular
        .module('DonationsApp')
        .directive('secondStep', secondStep)

        function secondStep(){
            return {
                restrict: 'E',
                scope: {
                    info: '='
                },
                templateUrl: 'js/app/directives/donation2/donation2.html',
                controller: donation2,
                controllerAs: 'secondStep'
            }
            donation2.$inject = ['$scope', 'DonationService']
            function donation2($scope, DonationService){
                vmS = this;
                //console.log($scope)
                vmS.listAgencies = $scope.info.listAgencies;
                vmS.selectAgency = selectAgency;
                vmS.changueSecond = changueSecond;

                function selectAgency(agency){
                    vmS.agency = agency;
                }

                function changueSecond(){
                    DonationService.setSecondStep(vmS.agency.DesignationEntityType, vmS.agency.EIN, vmS.agency.EntityId, vmS.agency.MinimumDonation, vmS.agency.MinimumTotalDonationForDesignation, vmS.agency.Name, vmS.agency.ProfileOrganizationNumber, vmS.agency.StandardAccountCode);
                    vmT.getData();
                }
                
            }

        }
})();