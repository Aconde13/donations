(function(){
    angular
        .module('DonationsApp')
        .controller('LoginController', loginController)

        loginController.$inject = ['AuthFactory', 'token', 'LoginFactory', 'LoginService', '$window']
        function loginController(AuthFactory, token, LoginFactory, LoginService, $window){
            vm = this;
            LoginService.setToken(token.data)
            vm.setData = checkData;

            function checkData(){
                if(vm.campingCode && vm.userName && vm.password){
                    vm.error = '';
                    var btL = document.getElementById('loginButton');
                    btL.innerHTML = '<i class="fas fa-pulse fa-2x fa-spinner"></i>'
                    setInfo();
                }else{
                    vm.error = 'Please, complete all info.'
                }
            }

            function setInfo(){
                LoginFactory.loginDonor(vm.campingCode, vm.userName, vm.password)
                    .then(function(response){
                        console.log(response)
                        if(response.data.Data.DonorToken){
                            var donor = response.data.Data.DonorToken;
                            LoginService.setDonor(donor)
                            $window.location.href = '#/newDonation'
                        }else{
                            vm.error = 'Please, try again.'
                            var btL = document.getElementById('loginButton'); 
                            btL.innerHTML = 'Accept'
                        }
                    });

            }

            

        }

        
})();