(function(){
    angular
        .module('DonationsApp')
        .controller('NewDonationController', newDonationController)

        newDonationController.$inject = ['LoginService', '$scope', 'DonationService', 'infoDonation', 'listCountries', 'listStates', 'listAgencies']
        function newDonationController(LoginService, $scope, DonationService, infoDonation, listCountries, listStates, listAgencies){
            vm = this;
            vm.jsonDonation = {
                'infoDonation': infoDonation.data.Data,
                'listCountries': listCountries.data.Data,
                'listStates': listStates.data.Data 
            }
            vm.jsonAgencies = {
                'listAgencies': listAgencies.data.Data
            }
            console.log(vm.jsonDonation)
            console.log(vm.jsonAgencies)
            
        }
})();