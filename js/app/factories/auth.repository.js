(function(){
    'use strict';
    angular
        .module('DonationsApp')
        .factory('AuthFactory', registerApp);

    registerApp.$insject = ['$http', 'apiKey', 'LoginService', 'api'];
    function registerApp($http, apiKey, LoginService, api){
        vm = this;

        return $http(
            {
                'method': 'GET', 
                'url': api+'/api/Application/Authenticate',
                'data' : {},
                'params' : {
                'apikey' : apiKey
                },
                'headers': {
                'Authorization': 'Basic dmlhcm86cEBzc3dvcmQ='
                }
            }).then(function(data){
                return data
            }).catch(function(err){
                return err;
            });
           

    }
})();