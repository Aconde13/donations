(function(){
    angular
        .module('DonationsApp')
        .directive('firstStep', firstStep)

        function firstStep(){
            return {
                restrict: 'E',
                scope: {
                    info: '='
                },
                templateUrl: 'js/app/directives/donation1/donation1.html',
                controller: donation1,
                controllerAs: 'firstStep'
            }
            donation1.$inject = ['$scope', 'DonationService', 'DonationFactory']
            function donation1($scope, DonationService, DonationFactory){
                vmF = this;
                vmF.infoDonation = $scope.info.infoDonation
                vmF.listCountries =  $scope.info.listCountries
                vmF.listStates = $scope.info.listStates
                vmF.minimumDonation = vmF.infoDonation.AmountQuestions[0].MinimumDonationAmount;
                vmF.changueTab = changueTab;
                vmF.changueInput = changueInput;
                vmF.getFrequency = getFrequency;
                vmF.getTypeCard = getTypeCard;
                vmF.setValuesRadio = setValuesRadio;
                vmF.checkCard = checkCard;

                function changueTab(){
                    getValues();
                    DonationService.setFirstStep(vmF.amount, vmF.frequency, vmF.cardType, vmF.cardNumber, vmF.cvn, vmF.nameOnCard, vmF.expirationDate, vmF.email, vmF.country, vmF.address1, vmF.address2, vmF.city, vmF.province, vmF.postalCode, vmF.zipCode, vmF.zipCodeExtension, vmF.state)
                }

                function changueInput(){
                    DonationService.changueInput();
                }

                function getFrequency(value){
                    switch(value) {
                        case 1:
                            return 'One-time';
                        break;
                        case 2:
                            return 'Monthly';
                        break;
                        case 3:
                            return 'Quartely';
                        break;
                        case 4:
                            return 'Semi-annual';
                        break;
                    }
                }

                function getTypeCard(value){
                    switch(value) {
                        case 1:
                            return 'Visa';
                        break;
                        case 2:
                            return 'Master Card';
                        break;
                        case 3:
                            return 'American Express';
                        break;
                        case 4:
                            return 'Discover';
                        break;
                        case 5:
                            return 'Diners Club';
                        break;
                    }
                }

                function getValues(){
                    if(document.getElementById('checkCustom').checked){
                        vmF.amount = parseInt(document.getElementById('inputDonation').value)
                    }
                }

                function setValuesRadio(type, data){
                    //type= 1: amount, 2: frequency
                    if(type == 1){
                        if(data != -1){
                            vmF.amount = data
                        }
                    }else{
                        vmF.frequency = data
                    }
                }

                function checkCard(e){
                    if(vmF.cardNumber){
                        //console.log(vmF.cardNumber)
                        DonationService.checkCard(vmF.cardNumber, vmF.cardType);
                    }else{
                        //console.log(e.key)
                        DonationService.checkCard(e.key, vmF.cardType);
                    }
                    
                }

            }

        }
})();