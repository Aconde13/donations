(function(){
    angular
        .module('DonationsApp')
        .directive('thirdStep', thirdStep)
        thirdStep.$inject = ['DonationFactory']
        function thirdStep(DonationFactory){
            return {
                restrict: 'E',
                scope: {
                    info: '='
                },
                templateUrl: 'js/app/directives/donation3/donation3.html',
                controller: donation3,
                controllerAs: 'thirdStep'
            }
            donation3.$inject = ['$scope', 'DonationService']
            function donation3($scope, DonationService){
                 vmT = this;
                 vmT.getData = getData;
                 vmT.getDate = getDate;
                 vmT.getTotalAmount = getTotalAmount;
                 vmT.saveDonation = saveDonation;

                function getData(){
                    vmT.dataFirst = DonationService.getFirstStep();
                    vmT.dataSecond = DonationService.getSecondStep();
                    vmT.PaymentTypeLabel = vmF.infoDonation.PaymentTypeLabel;
   
                    console.log(vmT)
                }

                function getDate(){
                    return new Date();
                }

                function getTotalAmount(){
                    if(vmT.dataFirst){
                        let amount = vmT.dataFirst.amount;
                        let frequency = vmT.dataFirst.frequency;
                        switch (frequency) {
                            case 1:
                                return amount
                                break;
                            case 2:
                                return amount*12
                                break;
                            case 3:
                                return amount*4
                                break;
                            case 4:
                                return amount*2
                                break;
                        }
                    }
                }

                function saveDonation(){
                    DonationFactory.setDonation().then((p)=>{
                        console.log(p)
                        if(p.status == 200){
                            vmC.changueStatus(1)
                        }else{
                            vmC.changueStatus(2)
                        }
                        $('#3').removeClass('active in');
                        $('#4').addClass('active in');
                        document.getElementById("myForm").reset()
                        document.getElementById("myForm2").reset()
                    }).catch((err)=>{
                        vmC.changueStatus(2)
                    });
                }
                 
            }

        }
})();