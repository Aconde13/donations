(function(){
    angular
        .module('DonationsApp')
        .service('DonationService', donationService)
    donationService.$inject = ['$cookies']
    function donationService($cookies){
        vm = this;
        vm.setFirstStep = setFirstStep;
        vm.setSecondStep = setSecondStep;
        vm.getFirstStep = getFirstStep;
        vm.getSecondStep = getSecondStep;
        vm.changueInput = changueInput;
        vm.checkAmount = checkAmount;
        vm.checkCard = checkCard;
        vm.checkZip = checkZip;

        function setFirstStep(amount, frequency, cardType, cardNumber, cvn, nameOnCard, expirationDate, email, country, address1, address2, city, province, postalCode, zipCode, zipCodeExtension, state){
            vm.firstStep = {
                'amount': amount,
                'frequency': frequency,
                'cardType': cardType,
                'cardNumber': cardNumber,
                'cvn': cvn,
                'nameOnCard': nameOnCard,
                'expirationDate': expirationDate,
                'email': email,
                'country': country,
                'address1': address1,
                'address2': address2,
                'city': city,
                'province': province,
                'postalCode': postalCode,
                'state': state,
                'zipCode': zipCode,
                'zipCodeExtension':zipCodeExtension
            }
        }

        function setSecondStep(designateableEntityType, ein, entityId, minimumDonation, minimumTotalDonationForDesignation, name, organizationNumber, standardAcountCode){
            vm.secondStep = {
                'designateableEntityType': designateableEntityType,
                'ein': ein,
                'entityId': entityId,
                'minimumDonation': minimumDonation,
                'minimumTotalDonationForDesignation': minimumTotalDonationForDesignation,
                'name': name,
                'organizationNumber': organizationNumber,
                'standardAccountCode': standardAcountCode
            }
        }

        function getFirstStep(){
            return vm.firstStep;
        }

        function getSecondStep(){
            return vm.secondStep;
        }

        function changueInput(){
            if(document.getElementById('checkCustom').checked){
                document.getElementById('inputDonation').disabled=false;
            }else{
                document.getElementById('inputDonation').disabled=true;
                document.getElementById('inputDonation').value = '';
            }
        }

        function checkAmount(amount, minimumDonation){
            if(amount > 0 && amount > minimumDonation){
                return true
            }else{
                return false
            }
        }

        function checkCard(cardNumber, typeCard){
                if (typeCard == 1) {
                    vm.pattern = /^4[0-9]{15}$/;
                } else if (typeCard == 2) {
                    vm.pattern = /^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$/;
                } else if (typeCard == 3) {
                    vm.pattern = /^3[47][0-9]{13}$/;
                } else if (typeCard == 4) {
                    vm.pattern = /^6(?:011|5[0-9]{2})[0-9]{12}$/;
                } else if (typeCard == 5) {
                    vm.pattern = /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/;
                }
    
                if (vm.pattern.test(cardNumber)) {
                    document.getElementById('spanCard').innerHTML = 'Tarjeta valida';    
                }else{
                    document.getElementById('spanCard').innerHTML = 'Tarjeta no valida!';
                }
                //console.log(vm.pattern.test(cardNumber));
        }

        function checkZip(zipCode, zipCodeExtension){
            if(zipCode == 5){
                if(zipCodeExtension){
                    if(zipCodeExtension == 4){
                        return true
                    }else{
                        return false
                    }
                }else{
                    return true
                }
            }else{
                return false
            }
        }
    }
})();