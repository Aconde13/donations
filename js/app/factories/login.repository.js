(function(){
    'use strict';
    angular
        .module('DonationsApp')
        .factory('LoginFactory', loginFactory)

    loginFactory.$inject = ['$http', 'apiKey', 'LoginService', 'api']
    function loginFactory($http, apiKey, LoginService, api){
        vm = this;

        return {
            loginDonor: function loginDonor(campingCode, username, password){
                return $http(
                    {
                        'method': 'GET', 
                        'url': api+'/api/Donor/Authenticate',
                        'data' : {},
                        'params' : {
                            'apikey' : apiKey,
                            'username': username,
                            'password': password,
                            'campaignCode': campingCode
                        
                        },
                        'headers': {
                        'Authorization': 'Bearer "'+ LoginService.getToken()+'"',
                        'apiKey': apiKey
                        }
                    }).then(function(data){
                        return data;
                    }).catch(function(err){
                        console.log()
                        return err;
                    });
            }
        }

    }
})();