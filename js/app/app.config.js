(function(){
    'use strict';
    angular
        .module('DonationsApp')
        .config(setRoutes)
        .constant('api', 'http://56fb228a.ngrok.io/OPPS.Web.API')
        .constant('apiKey', 'RTJ4+E4067P+mqIEizOkGu7nHAxRBfZzkE+/+r1/SbQ=');
        
    setRoutes.$inject = ['$routeProvider'];

    function setRoutes($routeProvider){
        $routeProvider
            .when('/login', {
                controller: 'LoginController',
                controllerAs: 'login',
                templateUrl: 'js/app/login/login.html',
                resolve: {
                    token: function(AuthFactory){
                        return AuthFactory.then();
                        //return {data:'TokenPrueb'}
                    }
                }
            })
            .when('/newDonation', {
                controller: 'NewDonationController',
                controllerAs: 'newDonation',
                templateUrl: 'js/app/newDonation/newDonation.html',
                resolve: {
                    infoDonation: function(DonationFactory){
                        //console.log(DonationFactory.getPaymentTypeConfiguration().then())
                        return DonationFactory.getPaymentTypeConfiguration().then();
                        //return {data:'TokenPrueb'}
                    },
                    listCountries: function(DonationFactory){
                        //console.log(DonationFactory.getContries().then())
                        return DonationFactory.getContries().then();
                        //return {data:'TokenPrueb'}
                    },
                    listStates: function(DonationFactory){
                        //console.log(DonationFactory.getStates().then())
                        return DonationFactory.getStates().then();
                        //return {data:'TokenPrueb'}
                    },
                    listAgencies: function(DonationFactory){
                        //console.log(DonationFactory.getStates().then())
                        return DonationFactory.getAgencies().then();
                        //return {data:'TokenPrueb'}
                    }
                }
            })
            .when('/listDonations', {
                controller: 'ListDonations',
                controllerAs: 'list',
                templateUrl: 'js/app/listDonations/listDonations.html',
                resolve: {
                    listDonations: function(DonationFactory){
                        return DonationFactory.getDonations().then();
                    }
                }
            })
            .when('/prueba',{
                resolve: {
                    donation: function(DonationFactory){
                        DonationFactory.setDonation().then((p)=>{
                            return p;
                        }).catch((err)=>{
                            return err;
                        });
                        
                        //return {data:'TokenPrueb'}
                        
                    }
                }
            });
    }
})();